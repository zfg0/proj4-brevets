**Author:** Zane Globus-O'Harra

**Contact:** zfg@uoregon.edu

### Brevet Calculator

This calculates the opening and closing control times for an ACP Brevet.
It takes a brevet distance and control locations. It then outputs the 
opening and closing control times based on the minimum and maximum speeds
laid out in [these rules](https://rusa.org/pages/acp-brevet-control-times-calculator).


*Notes:* I was unable to get the frontend to function as desired. I'm not 
sure what exactly I needed to add to make it so that acp_times.py properly
talked with calc.html. I tried adding something like 
```
var distance = ("#brevet_dist_km").val();
var start = ("#begin_date").val();
```
into the function on lines 130-140 in calc.html, but I did not succeed.