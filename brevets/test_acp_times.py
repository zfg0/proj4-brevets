import nose
import acp_times

def test_easy_open_time():
    """test a control below the minimum distance"""
    open_time = acp_times.open_time(60, 200, "2021-11-09T12:00:00-08:00")
    expected_time = "2021-11-09T13:45:52.941176-08:00"
    # print(open_time)
    print(open_time)
    assert open_time == expected_time


def test_harder_open_time():
    """test 300 < control < 400 < distance = 600"""
    open_time = acp_times.open_time(350, 600, "2021-11-09T12:00:00.000000-08:00")
    expected_time = "2021-11-09T22:34:11.470588-08:00"
    # print(open_time)
    print(open_time)
    assert open_time == expected_time


def test_hardest_open_time():
    """test a control near the maximum distance"""
    open_time = acp_times.open_time(890, 1000, "2021-11-09T12:00:00.000000-08:00")
    expected_time = "2021-11-10T17:09:22.184874-08:00"
    # print(open_time)
    print(open_time)
    assert open_time == expected_time


def test_close_0km_control():
    """test a 0km control close time (oddity)"""
    close_time = acp_times.close_time(0, 600, "2021-11-09T12:00:00.000000-08:00")
    expected_time = "2021-11-09T12:00:00-08:00"
    # print(close_time)
    print(close_time)
    assert close_time == expected_time


def test_easy_close_time():
    """test """
    close_time = acp_times.close_time(60, 600, "2021-11-09T12:00:00.000000-08:00")
    expected_time = "2021-11-09T16:00:00-08:00"
    # print(close_time)
    print(close_time)
    assert close_time == expected_time


def test_harder_close_time():
    """"""
    close_time = acp_times.close_time(890, 1000, "2021-11-09T12:00:00.000000-08:00")
    expected_time = "2021-11-12T05:22:34.567728-08:00"
    # print(f"close_time: {close_time}")
    print(close_time)
    assert close_time == expected_time
